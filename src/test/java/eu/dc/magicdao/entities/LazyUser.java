package eu.dc.magicdao.entities;

import eu.dc.magicdao.annotations.*;
import eu.dc.magicdao.annotations.loadoption.Load;
import eu.dc.magicdao.util.enums.Relation;

import java.util.*;

@Table(tablename = "U_USERS")
public class LazyUser {
    @Column(fieldname = "u_id", primarykey = true)
    public int id;
    @Column(fieldname = "u_username")
    public String username;
    @Column(fieldname = "u_password")
    public String password;
    @Column(fieldname = "u_favday")
	public Date favDay;

    @Load
    @Join public Setting setting;

    @Column(fieldname = "u_i_interests")
    @Load @Join(relation = Relation.OneToOne) public Interest interests;
    /*onetomanytest
    @Column(fieldname = "u_i_interests")
    @Load @Join(relation = Relation.OneToMany) public List<Interest> manyinterests;*/

    public LazyUser(String username, String password, Date favDay) {
        this.username = username;
        this.password = password;
		this.favDay = favDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}