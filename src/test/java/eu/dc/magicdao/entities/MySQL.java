package eu.dc.magicdao.entities;

import java.sql.*;

public class MySQL {
    private final String username;
    private final String password;
    private final String host;
    private final String database;
    private Connection connection;
    private static MySQL instance;

    public MySQL(String host, String username, String password, String database) {
        instance = this;
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public boolean initPool() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, username, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public void close() {
        try {
            if (this.connection != null) {
                connection.close();
            } else {
                System.err.println("Already closed.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ResultSet query(String query) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            if (statement.execute()) {
                ResultSet rs = statement.getResultSet();
                return rs;
            }
        } catch (SQLException ex) {
            System.out.println("Could not execute: " + query);
            ex.printStackTrace();
            throw new RuntimeException();
        }
        return null; //Querys which have no result return null
    }

    public Connection getConnection() {
        return this.connection;
    }

    public static MySQL getInstance() {
        return instance;
    }
}