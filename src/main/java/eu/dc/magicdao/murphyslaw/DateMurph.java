package eu.dc.magicdao.murphyslaw;

import eu.dc.magicdao.interfaces.AutoMorph;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dc on 23.02.2016.
 */
public class DateMurph implements AutoMorph{
    @Override
    public Date morph(String s)
    {
        return new Date(s);
    }

    @Override
    public String deMorph(Object date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
