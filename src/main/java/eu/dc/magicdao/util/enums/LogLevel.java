package eu.dc.magicdao.util.enums;

/**
 * Created by dc on 15.01.2016.
 */
public enum LogLevel {
    EMERGENCY(0), WARNING(1), DEBUG(2);

    int lvl;

    LogLevel(int lvl) {
        this.lvl=lvl;
    }

    public int getLvl(){
       return lvl;
    }
}
