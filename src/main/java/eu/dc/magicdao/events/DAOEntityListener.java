package eu.dc.magicdao.events;

import eu.dc.magicdao.DAOEntity;

/**
 * Created by dc on 11.04.2016.
 */
public interface DAOEntityListener {
    void newDAOEntityCreatedEvent(DAOEntity entity);
}
