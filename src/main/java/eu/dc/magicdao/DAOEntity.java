package eu.dc.magicdao;


import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOEntity {
    public String table_name;
    public ArrayList<Field> fields;
    public ArrayList<Field> primary_keys = new ArrayList<Field>();


    private DAOEntity(String table_name, ArrayList<Field> fields) {
        this.table_name = table_name;
        this.fields = fields;
        loadPrimaryKeys();

    }


    private void loadPrimaryKeys() {
        for (Field field : fields) {
            if (isPrimarykey(field)) {
                primary_keys.add(field);
            }
        }
    }

    public String getFieldName(Field field) {
        if (field.isAnnotationPresent(Column.class))
            return field.getAnnotation(Column.class).fieldname();
        throw new RuntimeException("This field has not the Fill annotation!");
    }


    private boolean isPrimarykey(Field field) {
        return field.getAnnotation(Column.class).primarykey();
    }

    public static DAOEntity makeDaoEntity(Class c) {
        String fieldname = ((Table) c.getAnnotation(Table.class)).tablename();
        ArrayList<Field> fields = new ArrayList<>();
        for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(Column.class)) {
                fields.add(f);
            }
        }
        return new DAOEntity(fieldname, fields);
    }
}