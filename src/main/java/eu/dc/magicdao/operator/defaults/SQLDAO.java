package eu.dc.magicdao.operator.defaults;

import eu.dc.magicdao.*;
import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Join;
import eu.dc.magicdao.annotations.Table;
import eu.dc.magicdao.interfaces.DAOOperator;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;
import eu.dc.magicdao.util.enums.Relation;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by me on 10.04.2016.
 */
public class SQLDAO implements DAOOperator{

    HashMap<DAOEntity,SQLPreparedStatements> preparedStatementsCache = new HashMap<>();
    Connection connection;
    SQLOperations execute;
    public static ConcurrentHashMap<Class, Class> proxysroot = new ConcurrentHashMap<>();
    private static ArrayList<Object> loadedBuggy = new ArrayList<>();

    public SQLDAO(Connection connection){
        this.connection=connection;
        this.execute=new SQLOperations(connection);
        initEvent();
    }


    void initEvent(){
        EventHandler.registerDaoEntityListener(entity -> {
            SQLPreparedStatements preparedStatements = new SQLPreparedStatements(entity,execute);
            preparedStatements.prepareStatements();
            preparedStatementsCache.put(entity,preparedStatements);
        });
    }

    @Override
    public boolean insert(Object target) {
        if (!execute.isConnectionReady())
            return false;
        if (!DAO.prePareForAction(target.getClass()))
            return false;
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());

        int i = 1;
        for (Field f : dao.fields) {
            try {
                if(Morphium.registeredMorphs.containsKey(f.getType()))
                {
                    System.out.println(Morphium.registeredMorphs.get(f.getType()).deMorph(f.get(target)));
                    preparedStatementsCache.get(dao).insert.setString(i, Morphium.registeredMorphs.get(f.getType()).deMorph(f.get(target)));
                }
                else
                    preparedStatementsCache.get(dao).insert.setString(i, f.get(target).toString());
                i++;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            preparedStatementsCache.get(dao).insert.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Object loadJoin(Field rootField, Object rootObject, Object target, Object value) {
        if (!execute.isConnectionReady())
            return false;
        if (!DAO.prePareForAction(proxysroot.get(target.getClass())))
            return false;
        /*if (!prePareForAction(proxysroot.get(target.getClass())))
        {
            Log.write("Couldnt load Proxy "+target.getClass().getName()+" / source "+proxysroot.get(target.getClass()).getName(),LogLevel.DEBUG);
            return false;
        }*/


        DAOEntity rootEntity = DAO.registeredClasses.get(rootObject.getClass());
        DAOEntity targetEntity = DAO.registeredClasses.get(proxysroot.get(target.getClass()));

        String rootTableName = rootEntity.table_name;
        String rootColumnName = rootEntity.getFieldName(rootField);

        String destTableName = targetEntity.table_name;
        String destColumnName = targetEntity.getFieldName(targetEntity.primary_keys.get(0));

        List<Object> list = new ArrayList<>();
        try {

            String query = "SELECT "+destTableName+".*"+" FROM "+rootTableName+" " +
                    "INNER JOIN "+destTableName+" " +
                    "ON "+rootTableName+"."+rootColumnName+"="+destTableName+"."+destColumnName+" " +
                    "WHERE "+destTableName+"."+destColumnName+"='"+value+"';";//value= rootField.getInt(rootObject)
            Log.write(query,LogLevel.DEBUG);
            PreparedStatement loadJoin = connection.prepareStatement(query);
            ResultSet resultSet = loadJoin.executeQuery();

            Log.write(resultSet.toString(),LogLevel.DEBUG);

            while (resultSet.next()){
                try {
                    Object obj = target;
                    for (Field field : targetEntity.fields) {
                        field.setAccessible(true);
                        try
                        {
                            field.set(obj, resultSet.getObject(targetEntity.getFieldName(field)));
                        }
                        catch(Exception e)
                        {
                            if(Morphium.registeredMorphs.containsKey(field.getType()))
                                field.set(obj, Morphium.registeredMorphs.get(field.getType()).morph(
                                        (String)resultSet.getObject(targetEntity.getFieldName(field))));
                            else
                            {
                                if(field.isAnnotationPresent(Join.class)){
                                    ProxyFactory factory = new ProxyFactory();
                                    if(field.getAnnotation(Join.class).relation()== Relation.OneToOne)
                                        factory.setSuperclass(field.getType());
                                    else
                                        factory.setSuperclass((Class) field.getGenericType());

                                    final Object finalTarget = target;
                                    MethodHandler handler = (self, thisMethod, proceed, args) -> {
                                        Log.write("Name: " + finalTarget.getClass().getName(),LogLevel.DEBUG);
                                        if(!loadedBuggy.contains(self)){
                                            loadedBuggy.add(self);
                                            proxysroot.put(self.getClass(),field.getType());
                                            // self = DAO.loadJoin(field, target, self);
                                        }
                                        return proceed.invoke(self, args);
                                    };
                                    Object dog = factory.create(new Class<?>[0], new Object[0], handler);
                                    field.set(obj,dog);
                                }else{
                                    Log.write("no morphs & no joins found", LogLevel.DEBUG);
                                }
                            }
                        }
                    }
                    list.add(obj);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(rootField.getAnnotation(Join.class).relation()==Relation.OneToOne)
            target = list.get(0);


        return target;

    }

    @Override
    public Object load(Object target) {
        if (!execute.isConnectionReady())
            return false;
        if (!DAO.prePareForAction(target.getClass()))
            return false;

        DAOEntity dao = DAO.registeredClasses.get(target.getClass());
        ArrayList<Field> primary_key = dao.primary_keys;
        try {
            int i = 1;
            for (Field f : primary_key) {
                try {
                    preparedStatementsCache.get(dao).load.setString(i, f.get(target).toString());
                    i++;
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            Log.write(preparedStatementsCache.get(dao).load.toString(), LogLevel.DEBUG);
            ResultSet resultSet = preparedStatementsCache.get(dao).load.executeQuery();
            Log.write(resultSet.toString(),LogLevel.DEBUG);
            try {
                resultSet.next();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            try {
                Object obj = target;
                for (Field field : dao.fields) {
                    field.setAccessible(true);
                    try
                    {
                        field.set(obj, resultSet.getObject(dao.getFieldName(field)));
                    }
                    catch(Exception e)
                    {
                        if(Morphium.registeredMorphs.containsKey(field.getType()))
                            field.set(obj, Morphium.registeredMorphs.get(field.getType()).morph(
                                    (String)resultSet.getObject(dao.getFieldName(field))));
                        else
                        {
                            if(field.isAnnotationPresent(Join.class)){
                                ProxyFactory factory = new ProxyFactory();
                                factory.setSuperclass(field.getType());

                                MethodHandler handler = (self, thisMethod, proceed, args) -> {
                                    if(!loadedBuggy.contains(self.getClass())){
                                        proxysroot.put(self.getClass(), field.getType());
                                        Log.write("FIRST Lazy load: "+self.getClass().getName()+ "/"+proxysroot.get(self.getClass()).getName(),LogLevel.DEBUG);
                                        loadedBuggy.add(self.getClass());
                                        if(field.getAnnotation(Join.class).relation() == Relation.OneToOne)
                                            self = loadJoin(field, target, self, resultSet.getObject(dao.getFieldName(field)));

                                    }
                                    return proceed.invoke(self, args);
                                };
                                Object dog = factory.create(new Class<?>[0], new Object[0], handler);
                                field.set(obj,dog);
                            }else{
                                Log.write("no morphs & no joins found", LogLevel.DEBUG);
                            }
                        }
                    }
                }
                return obj;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean update(Object target) {
        if (!execute.isConnectionReady())
            return false;
        if (!DAO.prePareForAction(target.getClass()))
            return false;
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());

        int x = 1;
        for (int i = 0; i < dao.fields.size(); i++) {
            try {
                dao.fields.get(i).setAccessible(true);
                if (dao.fields.get(i).isAnnotationPresent(Column.class)) {
                    // dao.update.setString(x, dao.fields.get(i).get(target).toString());
                    if(Morphium.registeredMorphs.containsKey(dao.fields.get(i).getType()))
                    {
                        preparedStatementsCache.get(dao).update.setString(x, Morphium.registeredMorphs.get(dao.fields.get(i).getType()).deMorph(dao.fields.get(i).get(target)));
                    }
                    else
                        preparedStatementsCache.get(dao).update.setString(x, dao.fields.get(i).get(target).toString());
                    x++;
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        for (int i = 0; i < dao.fields.size(); i++) {
            try {
                dao.fields.get(i).setAccessible(true);
                if (dao.fields.get(i).getAnnotation(Column.class).primarykey()) {
                    if(Morphium.registeredMorphs.containsKey(dao.fields.get(i).getType()))
                    {
                        preparedStatementsCache.get(dao).update.setString(x, Morphium.registeredMorphs.get(dao.fields.get(0).getType()).deMorph(dao.fields.get(i).get(target)));
                    }
                    else
                        preparedStatementsCache.get(dao).update.setString(x, dao.fields.get(i).get(target).toString());
                    x++;
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        try {
            preparedStatementsCache.get(dao).update.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public Object findby(Object target, Field... fieldie) {
        if (!execute.isConnectionReady())
            return false;
        if (!DAO.prePareForAction(target.getClass()))
            return false;

        DAOEntity dao = DAO.registeredClasses.get(target.getClass());
        if (fieldie.length < 1)
            return false;
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM ").append(dao.table_name);
        builder.append(" WHERE ");
        for (Field f : fieldie) {
            builder.append(f.getAnnotation(Column.class).fieldname());
            builder.append("='");
            try {
                f.setAccessible(true);
                builder.append(f.get(target).toString());
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            builder.append("' AND ");
        }
        String text = builder.substring(0, builder.length() - 4) + ";";
        System.out.println(text);
        ResultSet resultSet = execute.execute(text);
        try {
            resultSet.next();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            Object obj = target;
            for (Field field : dao.fields) {
                field.setAccessible(true);
                field.set(obj, resultSet.getObject(dao.getFieldName(field)));
            }
            return obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> findAll(Class c) {
        if (DAO.registeredClasses.get(c) != null) {
            ResultSet result = execute.execute("SELECT * FROM " +
                    ((Table) c.getAnnotation(Table.class)).tablename());
        }
        throw new RuntimeException("Missing Annotation at " + c.getName());
    }
}
