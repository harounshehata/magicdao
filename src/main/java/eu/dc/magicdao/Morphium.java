package eu.dc.magicdao;

import eu.dc.magicdao.interfaces.AutoMorph;
import eu.dc.magicdao.murphyslaw.ColorMurph;
import eu.dc.magicdao.murphyslaw.DateMurph;

import java.awt.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by dc on 22.02.2016.
 */
public class Morphium {
    public static ConcurrentHashMap<Class, AutoMorph> registeredMorphs = new ConcurrentHashMap<>();
    public static void defaultMorphes(){
        Morphium.insertMorph(Date.class, new DateMurph());
        Morphium.insertMorph(Color.class, new ColorMurph());
    }

    public static void insertMorph(Class c, AutoMorph morph){
        registeredMorphs.put(c,morph);
    }

    public static void removeMorph(Class c){
        registeredMorphs.remove(c);
    }

}
